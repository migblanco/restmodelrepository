package afi.repository.utils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class ManagementRegisterCRS {
	private static String DIRSAVE = System.getProperty("dir.save")+"/Components";
	private static String swcRegister = "SWC_register";
	

	public static JSONArray getJSONArray() throws IOException {
		JSONArray jsonArray = null;
		
		File jsonFile;

			jsonFile = new File(DIRSAVE + "/" + swcRegister + ".json");
			if (!jsonFile.exists())
				createRootRegister();

			jsonArray = new JSONArray(ManagementFiles.readFile(jsonFile));
		
			return jsonArray;

		}


	public static void createRootRegister() throws IOException {
		int counterCM = 0;
		JSONArray jsonAppArray = new JSONArray();
		JSONObject swccont = new JSONObject();
		JSONObject content = new JSONObject();
		content.put("Content", "Software Components");
		swccont.put("Counter", counterCM);
		jsonAppArray.put(content);
		jsonAppArray.put(swccont);
		saveRegisterJson(swcRegister, jsonAppArray);

	}

	public static void registerJSON(String comp_id, String version) throws JSONException, IOException {

		int counterSWC = 0;
		JSONArray jsonCompArray = getJSONArray();

		JSONObject swccont = new JSONObject();
		JSONObject swc = new JSONObject();
		JSONObject auxiliar = new JSONObject();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String currentTimeStamp = sdf.format(timestamp);

		for (int i = 0; i < jsonCompArray.length(); i++) {

			auxiliar = jsonCompArray.getJSONObject(i);

			Iterator<String> iter = auxiliar.keys();

			while (iter.hasNext()) {

				String obj = (String) iter.next();

				if (obj.equals("Counter")) {

					counterSWC = auxiliar.getInt("Counter");
					counterSWC++;
					jsonCompArray.remove(i);

				}

			}
		}
		swc.put("Comp_id", comp_id);
		swc.put("SWC_version", version);
		swc.put("Timestamp", currentTimeStamp);
		swc.put("SWC_id", counterSWC);
		swccont.put("Counter", counterSWC);
		jsonCompArray.put(swccont);
		jsonCompArray.put(swc);
		saveRegisterJson(swcRegister, jsonCompArray);

	}

	public static void saveRegisterJson(String register, JSONArray objson) throws IOException {

		
		File jsonFile = new File(DIRSAVE + "/" + register + ".json");

		FileWriter fileWriter = new FileWriter(jsonFile);
		fileWriter.write(objson.toString());

		fileWriter.close();

		System.out.println("Registrado como " + DIRSAVE + "/" + register + ".json");
	}


	public static void updateAfterDelete(String comp_id, String versionDeleted) throws IOException {

		JSONArray jsonArray = getJSONArray();
		
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject auxiliar = jsonArray.getJSONObject(i);
			Iterator<String> iter = auxiliar.keys();

			while (iter.hasNext()) {

				String obj = (String) iter.next();

				if (obj.equals("SWC_version") && auxiliar.getString("SWC_version").equals(versionDeleted))

					if(auxiliar.getString("Comp_id").equals(comp_id)) jsonArray.remove(i);
			}
			
		}
		saveRegisterJson(swcRegister, jsonArray);

		
	}

}
//System.out.println();