package afi.repository.utils;

import java.io.File;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;


public class ManagementJSON {

	public static Object createCMJsonToSend(Object component) {

		File auxiliar = new File(component.toString());

		String componentModel = ManagementFiles.readFile(auxiliar);

		JSONObject objson = new JSONObject();

		try {
			objson.put("CM", componentModel);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return objson;
	}

	public static Object createSCMJsonToSend(String app, Object component, String name) throws IOException {

		File auxSCM = new File(component.toString());

		String componentModel = ManagementFiles.readFile(auxSCM);

		String componentModelString = ManagementRegisterMRS.getAssociatedCMAsString(app, name);

		JSONObject objson = new JSONObject();

		try {
			objson.put("SCM", componentModel);
			objson.put("CM", componentModelString);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return objson;
	}

	public static Object createJsonToSend(Object component) throws IOException {


		Object componentModel = ManagementFiles.convertFile(component.toString());

		JSONObject objson = new JSONObject();

		try {
			objson.put("SWC", componentModel);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return objson;
	}
}

