package afi.repository.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Base64;
import org.json.JSONArray;

public class ManagementFiles {

	private static String DIRSAVE = System.getProperty("dir.save");

	public static void saveAsBinaryFile(String destPath, Object theModel) throws IOException {

		Base64.Decoder decoder = Base64.getMimeDecoder();
		byte[] decodedByteArray = decoder.decode(theModel.toString());

		try {
			FileOutputStream binaryObject = new FileOutputStream(destPath);

			binaryObject.write(decodedByteArray);
			binaryObject.close();
		} catch (IOException ex) {
			ex.printStackTrace();

		}

		System.out.println("Guardado como " + destPath);
	}

	public static void saveJsonFile(String name, String destPath, JSONArray objson) throws IOException {

		FileWriter fileWriter = new FileWriter(destPath + "/" + name + ".json");
		fileWriter.write(objson.toString());

		fileWriter.close();

		System.out.println("Guardado como " + destPath + "/" + name + ".json");
	}

	public static String readFile(File f) {
		String fileContent = "";

		if (f == null)
			fileContent = "Not Found(404) - The requested resource could not be found but may be available in the future";

		else {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				String inputLine;
				while ((inputLine = br.readLine()) != null) {
					fileContent += inputLine;
				}
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fileContent;
	}

	public static File getFilefromRepository(String app, String version) {

		File cmFile;

		String path = DIRSAVE + "/" + app + "/cm/" + version;

		File rep = new File(path);

		if (!rep.isDirectory()) {

			cmFile = null;

		} else {
			File[] arrayFile = rep.listFiles();

			cmFile = arrayFile[0];
		}
		return cmFile;
	}

	public static Object convertFile(String name) throws IOException {

		File file = new File(System.getProperty("java.io.tmpdir") + "/theModel.dat");
		File origin = new File(name);
		byte[] buffer = null;

		try (InputStream inputStream = new FileInputStream(origin);
				OutputStream outputStream = new FileOutputStream(file);) {

			int buffersize = (int) origin.length();
			buffer = new byte[buffersize];

			while (inputStream.read(buffer) != -1) {
				outputStream.write(buffer);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return encodeFile(buffer);
	}

	public static Object encodeFile(byte[] bytes) throws IOException {

		Base64.Encoder mimeEncoder = Base64.getMimeEncoder();
		String encode = mimeEncoder.encodeToString(bytes);
		return encode;
	}

	public static void saveAsFile(String destPath, Object theModel) throws IOException {

		BufferedWriter bufferedWriter = null;
		try {
			File myFile = new File(destPath);

			if (!myFile.exists()) {
				myFile.createNewFile();
			}
			Writer writer = new FileWriter(myFile);
			bufferedWriter = new BufferedWriter(writer);
			bufferedWriter.write(theModel.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedWriter != null)
					bufferedWriter.close();
			} catch (Exception ex) {

			}
		}

		System.out.println("Guardado como " + destPath);
	}

}



