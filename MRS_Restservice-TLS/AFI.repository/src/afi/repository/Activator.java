package afi.repository;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import afi.interfaces.IComponentRepository;
import afi.interfaces.IModelRepository;

public class Activator implements BundleActivator {


	private static BundleContext context;
	
	static BundleContext getContext() {
		
		return context;
	}
	
	@SuppressWarnings("unchecked")
	public void start(BundleContext bundleContext) throws Exception {
	
		ServiceRegistration<IModelRepository> serv_mrs;
		Activator.context = bundleContext;
		IModelRepository mrsService = new ModelRepository();
		serv_mrs = (ServiceRegistration<IModelRepository>) bundleContext.registerService(IModelRepository.class.getName(), mrsService, null);
		if(serv_mrs != null) System.out.println("Arrancando repositorio MRS");
		ServiceRegistration<IComponentRepository> serv_crs;
		//Activator.context = bundleContext;
		IComponentRepository crsService = new ComponentRepository();
		serv_crs = (ServiceRegistration<IComponentRepository>) bundleContext.registerService(IComponentRepository.class.getName(), crsService, null);
		if(serv_crs != null) System.out.println("Arrancando repositorio CRS");
    }
    public void stop(BundleContext context) throws Exception {
    	Activator.context = null;
    	System.out.println("Parando repositorio");
    }
}
