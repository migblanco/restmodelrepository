package afi.repository;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import afi.repository.utils.ManagementFiles;
import afi.repository.utils.ManagementJSON;
import afi.repository.utils.ManagementRegister;
import es.upv.pros.tatami.autonomic.adaptation.framework.repository.interfaces.v1.IModelRepository;

public class ModelRepository implements IModelRepository {

	private String DIRSAVE = "";
	private String SAVEREGISTER="";

	public ModelRepository() {
		this.DIRSAVE = System.getProperty("dir.save");
		
	}

	@Override
	public Object saveComponentModel(String app, String version, Object theModel,String extension) {
       
		String destPath="";
		JSONObject respuesta = new JSONObject();
		File directory = new File(this.DIRSAVE + "/" + app + "/cm/" + version);
		try {
			if (!directory.isDirectory()) directory.mkdirs();
			
			destPath = directory.getAbsolutePath() + "/" + app + extension;
			
				if(extension.equals(".cm")) ManagementRegister.registerJSONCM( app, version);
				ManagementFiles.saveAsBinaryFile(destPath, theModel);


			respuesta.put("code", "201");

		} catch (IOException | JSONException e) {

			e.printStackTrace();

		}

		return respuesta;

	}

	@Override
	public Object getComponentModel(String app, String version) {

		JSONObject respuesta = new JSONObject();

		Object cmFile = new Object();

		String path = this.DIRSAVE + "/" + app + "/cm/" + version;

		File rep = new File(path);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return respuesta;
		} else {

			File[] arrayFile = rep.listFiles();

			cmFile = arrayFile[0];
		}

		return ManagementJSON.createCMJsonToSend(cmFile);
	}

	@Override
	public Object saveSystemConfigurationModel(String app, String name, String version, String cm_version,
			Object theModel) {
		JSONObject respuesta = new JSONObject();
		JSONArray objson = new JSONArray();
		File directory = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version);
		try {
		if (!directory.isDirectory()) {

			directory.mkdirs();

			String destPath = directory.getAbsolutePath() + "/" + name + ".scm";

				ManagementRegister.registerJSONSCM( app,name,version,cm_version);
				ManagementFiles.saveAsBinaryFile(destPath, theModel);
				respuesta.put("code", "201");

			
		} else {
			respuesta = (JSONObject) managementUpdates(app, name, version, cm_version, theModel);
		}
		} catch (JSONException | IOException e) {

			e.printStackTrace();
		}

		return respuesta;
	}

	@Override
	public Object getSystemConfigurationModel(String app, String name, String version) {

		JSONObject respuesta = new JSONObject();
		Object scmFile = new Object();
		Object scmJson = new Object();

		String path = this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version;

		File rep = new File(path);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return respuesta;

		} else {

			File[] arrayFile = rep.listFiles();

			for (int i = 0; i < arrayFile.length; i++) {

				int pos = arrayFile[i].getName().lastIndexOf(".");
				String resp = arrayFile[i].getName().substring(pos);
				if (resp.equals(".scm"))
					scmFile = arrayFile[i];

			}

		}
		try {
			scmJson= ManagementJSON.createSCMJsonToSend(app,scmFile,name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scmJson;
	}

	public Object managementUpdates(String app, String name, String version, String cm_version, Object theModel) throws IOException {

		Object respuesta = new Object();
		File archivoSCM = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version + "/" + name + ".scm");
		//File archivoJSON = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version + "/" + name + ".json");
		if (archivoSCM.exists() && !theModel.equals(""))
			respuesta = updateSystemConfigurationModel(app, name, version, theModel);
		else {
			
				respuesta = updateConfigurationModel(app, name, version, cm_version);
		}

		return respuesta;

	}

	public Object updateSystemConfigurationModel(String app, String name, String version, Object theModel) throws IOException {

		JSONObject respuesta = new JSONObject();

		// Almacenado de la nueva configuración

		File rep = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return respuesta;
		}

		String destPath = rep.getAbsolutePath() + "/" + name + ".scm";

		try {
			ManagementFiles.saveAsBinaryFile(destPath, theModel);

		} catch (IOException e) {

			e.printStackTrace();
		}

		// Actualización del archivo JSON

	
		try {
			//ManagementRegister.updateJSON(app,name,version,"SCM");

			respuesta.put("code", "200");

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return respuesta;

	}

	public Object updateConfigurationModel(String app, String name, String version, String cm_version) {

		JSONObject respuesta = new JSONObject();

		JSONArray objson = new JSONArray();

		File rep = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version);
		try {
		if (!rep.isDirectory()) {
			
				respuesta.put("error", "404");

			

			return respuesta;
		}

		// Actualización del archivo JSON

            ManagementRegister.updateJSON(app,name, cm_version,"CM");
			
			respuesta.put("code", "200");

		} catch (JSONException | IOException e1) {
			e1.printStackTrace();
		}

		return respuesta;

	}

	@Override
	public Object deleteComponentModel(String app, String version) {

		JSONObject respuesta = new JSONObject();

		String path = this.DIRSAVE + "/" + app + "/cm/" + version;

		File rep = new File(path);
		try {
			if (!rep.isDirectory()) {

				respuesta.put("error", "404");

				return respuesta;

			} else {

				File[] arrayFile = rep.listFiles();

				for (int i = 0; i < arrayFile.length; i++) {

					arrayFile[i].delete();
				}
				if (rep.delete())
					ManagementRegister.updateAfterDeleteCM(app, version);
				    respuesta.put("code", "200");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return respuesta;
	}

	@Override
	public Object deleteSystemConfigurationModel(String app, String name, String version) {
		JSONObject respuesta = new JSONObject();

		String path = this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version;

		File rep = new File(path);
		try {
			if (!rep.isDirectory()) {

				respuesta.put("error", "404");

			} else {
				File[] arrayFile = rep.listFiles();

				for (int i = 0; i < arrayFile.length; i++) {

					arrayFile[i].delete();
				}
				if (rep.delete()) {
					ManagementRegister.updateAfterDeleteSCM(app, name, version);
					respuesta.put("code", "200");
				}
			}
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return respuesta;
	}

}
