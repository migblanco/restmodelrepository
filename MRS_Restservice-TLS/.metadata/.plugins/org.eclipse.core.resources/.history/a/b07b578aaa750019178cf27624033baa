package afi.repository;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import afi.repository.utils.ManagementFiles;
import afi.repository.utils.ManagementJSON;
import afi.repository.utils.ManagementRegister;
import es.upv.pros.tatami.autonomic.adaptation.framework.repository.interfaces.v1.IModelRepository;

public class ModelRepository implements IModelRepository {

	private String DIRSAVE = "";
	private String SAVEREGISTER="";

	public ModelRepository() {
		this.DIRSAVE = System.getProperty("dir.save");
		
	}

	@Override
	public Object saveComponentModel(String app, String version, Object theModel,String extension) {
       
		String destPath="";
		JSONObject respuesta = new JSONObject();
		File directory = new File(this.DIRSAVE + "/" + app + "/cm/" + version);
		try {
			if (!directory.isDirectory()) {
				directory.mkdirs();

				ManagementRegister.createRootRegister(app);
				destPath = directory.getAbsolutePath() + "/" + app + extension;
				
				ManagementRegister.registerJSONCM( app, version);
				ManagementFiles.saveAsBinaryFile(destPath, theModel);
				
			}else {
				destPath = directory.getAbsolutePath() + "/" + app + extension;
			
				ManagementRegister.registerJSONCM( app, version);
				ManagementFiles.saveAsBinaryFile(destPath, theModel);
			}

			respuesta.put("code", "201");

		} catch (IOException | JSONException e) {

			e.printStackTrace();

		}

		return respuesta;

	}

	@Override
	public Object getComponentModel(String app, String version) {

		JSONObject respuesta = new JSONObject();

		Object cmFile = new Object();

		String path = this.DIRSAVE + "/" + app + "/cm/" + version;

		File rep = new File(path);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return respuesta;
		} else {

			File[] arrayFile = rep.listFiles();

			cmFile = arrayFile[0];
		}

		return ManagementJSON.createCMJsonToSend(cmFile);
	}

	@Override
	public Object saveSystemConfigurationModel(String app, String name, String version, String cm_version,
			Object theModel) {
		JSONObject respuesta = new JSONObject();
		JSONArray objson = new JSONArray();
		File directory = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version);

		if (!directory.isDirectory()) {

			directory.mkdirs();

			String destPath = directory.getAbsolutePath() + "/" + name + ".scm";

			try {
				objson = ManagementJSON.createJSON(cm_version, app, name, version);
				ManagementFiles.saveAsBinaryFile(destPath, theModel);
				ManagementFiles.saveJsonFile(name, directory.getAbsolutePath(), objson);
				respuesta.put("code", "201");

			} catch (JSONException | IOException e) {

				e.printStackTrace();
			}

		} else {
			respuesta = (JSONObject) managementUpdates(app, name, version, cm_version, theModel);
		}
		return respuesta;
	}

	@Override
	public Object getSystemConfigurationModel(String app, String name, String version) {

		JSONObject respuesta = new JSONObject();
		Object scmFile = new Object();
		Object scmJson = new Object();

		String path = this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version;

		File rep = new File(path);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return respuesta;

		} else {

			File[] arrayFile = rep.listFiles();

			for (int i = 0; i < arrayFile.length; i++) {

				int pos = arrayFile[i].getName().lastIndexOf(".");
				String resp = arrayFile[i].getName().substring(pos);
				if (resp.equals(".scm"))
					scmFile = arrayFile[i];
				else if (resp.equals(".json"))
					scmJson = arrayFile[i];

			}

		}
		return ManagementJSON.createSCMJsonToSend(scmFile, scmJson);
	}

	public Object managementUpdates(String app, String name, String version, String cm_version, Object theModel) {

		Object respuesta = new Object();
		File archivoSCM = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version + "/" + name + ".scm");
		File archivoJSON = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version + "/" + name + ".json");
		if (archivoSCM.exists() && !theModel.equals(""))
			respuesta = updateSystemConfigurationModel(app, name, version, theModel);
		else {
			if (archivoJSON.exists())
				respuesta = updateConfigurationModel(app, name, version, cm_version);
		}

		return respuesta;

	}

	public Object updateSystemConfigurationModel(String app, String name, String version, Object theModel) {

		JSONObject respuesta = new JSONObject();

		// Almacenado de la nueva configuración

		File rep = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return respuesta;
		}

		String destPath = rep.getAbsolutePath() + "/" + name + ".scm";

		try {
			ManagementFiles.saveAsBinaryFile(destPath, theModel);

		} catch (IOException e) {

			e.printStackTrace();
		}

		// Actualización del archivo JSON

		JSONArray objson = new JSONArray();

		File json = new File(rep.getAbsolutePath() + "/" + name + ".json");

		JSONArray objsonFile;
		try {
			objsonFile = new JSONArray(ManagementFiles.readFile(json));

			objson = ManagementJSON.updateJSONSCM(objsonFile);
			
			ManagementFiles.saveJsonFile(name, rep.getAbsolutePath(), objson);

			respuesta.put("code", "200");

		} catch (JSONException | IOException e1) {
			e1.printStackTrace();
		}
		return respuesta;

	}

	public Object updateConfigurationModel(String app, String name, String version, String cm_version) {

		JSONObject respuesta = new JSONObject();

		JSONArray objson = new JSONArray();

		File rep = new File(this.DIRSAVE + "/" + app + "/scm/" + name + "/" + version);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return respuesta;
		}

		// Actualización del archivo JSON

		File json = new File(rep.getAbsolutePath() + "/" + name + ".json");

		JSONArray objsonFile;
		try {
			objsonFile = new JSONArray(ManagementFiles.readFile(json));

			objson = ManagementJSON.updateJSONCM(cm_version, objsonFile);

			ManagementFiles.saveJsonFile(name, rep.getAbsolutePath(), objson);
			
			respuesta.put("code", "200");

		} catch (JSONException | IOException e1) {
			e1.printStackTrace();
		}

		return respuesta;

	}

	@Override
	public Object deleteComponentModel(String app, String version) {
		
		JSONObject respuesta = new JSONObject();

		String path = this.DIRSAVE + "/" + app + "/cm/" + version;

		File rep = new File(path);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");
				

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return respuesta;

		} else {
			File[] arrayFile = rep.listFiles();

			for (int i = 0; i < arrayFile.length; i++) {
				
				arrayFile[i].delete();
			}
			   if(rep.delete()) {
			   
			   respuesta.put("code", "200");
		}
			   }

		return respuesta;
	}

	@Override
	public Object deleteSystemConfigurationModel(String app, String name, String version) {
		JSONObject respuesta = new JSONObject();

		String path = this.DIRSAVE + "/" + app + "/scm/" + name+"/"+ version;

		File rep = new File(path);

		if (!rep.isDirectory()) {
			try {
				respuesta.put("error", "404");
				

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			File[] arrayFile = rep.listFiles();

			for (int i = 0; i < arrayFile.length; i++) {
				
				arrayFile[i].delete();
			}
			   if(rep.delete()) {
			   
			   respuesta.put("code", "200");
		}
			   }

		return respuesta;
	}

}
