package afi.interfaces;

public interface IComponentRepository {
	
	public Object saveSoftwareComponent(String comp_id, String version, Object fileToSave, String extension);

	public Object getSoftwareComponent(String comp_id, String version);

	public Object deleteSoftwareComponent(String comp_id, String version);

}
