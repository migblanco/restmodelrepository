package afi.interfaces;



public interface IModelRepository {
	
	
	// Components Models
	public Object saveComponentModel(String app, String version, Object fileToSave, String extension);

	public Object getComponentModel(String app, String version);

	public Object deleteComponentModel(String app, String version);

	// System Configuration Models
	public Object saveSystemConfigurationModel(String app, String name, String version, String cm_version,
			Object theModel);

	public Object getSystemConfigurationModel(String app, String name, String version);

	public Object deleteSystemConfigurationModel(String app, String name, String version);

}

