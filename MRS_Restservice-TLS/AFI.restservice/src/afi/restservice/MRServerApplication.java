package afi.restservice;


import org.osgi.framework.BundleContext;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import afi.restservice.logic.CRSSoftwareComponent;
import afi.restservice.logic.MRSComponentModel;
import afi.restservice.logic.MRSConfigurationModel;
import afi.restservice.logic.RootServerResource;


public class MRServerApplication extends Application{
	
private BundleContext context;
 
	
	
	public MRServerApplication (BundleContext context) {
		
		setName("RESTful MRS");
		setDescription("REST service on MAPE-K architecture");
		setOwner("MRS SAS");
		setAuthor("The MRS Team");

	}
	
	public BundleContext getBundleContext() {
		return this.context;
	}

	public Restlet createInboundRoot() {
	   
	    Router router = new Router(getContext());    
	    router.attach("/", RootServerResource.class);      
	    router.attach("/cmodel/{app}/{version}", MRSComponentModel.class); 
	    router.attach("/scmodel/{app}/{name}/{version}", MRSConfigurationModel.class);
	    router.attach("/component/{component_id}/{version}", CRSSoftwareComponent.class);
	    return router;
		
	}

}

