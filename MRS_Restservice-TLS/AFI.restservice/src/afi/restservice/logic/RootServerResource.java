package afi.restservice.logic;


import org.osgi.framework.BundleContext;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ServerResource;
import org.restlet.resource.ResourceException;

import afi.restservice.MRServerApplication;


public class RootServerResource extends ServerResource {

	protected BundleContext getBundleContext() {
	
		return ((MRServerApplication) this.getApplication()).getBundleContext();
	}

	
	

	public RootServerResource() {
		
		setNegotiated(false);
		//setExisting(true);
	}
   @Override
	protected void doInit() throws ResourceException{
		
		System.out.println("Recurso raiz fue iniciado");
		
		
	}
   @Override
	protected void doCatch(Throwable throwable) {
		
		System.out.println("Excepción lanzada");
	}
   @Override
	protected void doRelease() throws ResourceException {
		
		System.out.println("Recurso raiz fue lanzado");
	}
   @Override
	protected Representation get()throws ResourceException{
		
		System.out.println("El método get de la raiz fue invocado");
		return new StringRepresentation("Bienvenido a MRS_remoto");
	}
   @Override
	protected Representation options()throws ResourceException{
		
		System.out.println("El método options de la raiz fue invocado");
		throw new RuntimeException("Todavia no implementado");
	}
}