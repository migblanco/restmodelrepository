package afi.restservice.logic;

import java.io.FileNotFoundException;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import afi.restservice.RestActivator;

public class MRSConfigurationModel extends ServerResource {

	@Get("json")
	public JsonRepresentation getSystemConfigurationModel() {

		JSONObject respuesta = new JSONObject();

		String app = getAttribute("app");
		String name = getAttribute("name");
		String version = getAttribute("version");

		respuesta = (JSONObject) RestActivator.getSystemConfigurationModel(app, name, version);
		
		Iterator<String> iter = respuesta.keys();
		String obj = (String) iter.next();
		if (obj.equals("error")) {
			// setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			return new JsonRepresentation(respuesta.toString());
		} else {
			return new JsonRepresentation(respuesta.toString());

		}
	}

	@Put("json")
	public void saveSystemConfigurationModel(JsonRepresentation jsonrepresentation)
			throws JSONException, FileNotFoundException {
		JSONArray requestJSONArray = jsonrepresentation.getJsonArray();
		JSONObject respuesta = new JSONObject();

		for (int i = 0; i < requestJSONArray.length(); i++) {
			JSONObject requestJSON = requestJSONArray.getJSONObject(i);
			
			String APP = requestJSON.getString("App");
			String name = requestJSON.getString("SCM_name");
			String version = requestJSON.getString("SCM_version");
			String cm_version = requestJSON.getString("CM_version");
			String theModel = requestJSON.getString("SCM");

			respuesta = (JSONObject) RestActivator.saveSystemConfigurationModel(APP, name, version, cm_version,
					theModel);

			Iterator<String> iter = respuesta.keys();
			String obj = (String) iter.next();
			if (obj.equals("code")) {

				String code = respuesta.getString("code");

				switch (code) {
				case "201":
					setStatus(Status.SUCCESS_CREATED);
					break;
				case "200":
					setStatus(Status.SUCCESS_OK);
					break;
				default:
					break;
				}
			}

		}

	}
	@Delete("json")
	public JsonRepresentation deleteSystemConfigurationModel() {
		
		JSONObject respuesta = new JSONObject();

		String app = getAttribute("app");
		String name = getAttribute("name");
		String version = getAttribute("version");
		
		respuesta = (JSONObject) RestActivator.deleteSystemConfigurationModel(app, name, version);
	
		Iterator<String> iter = respuesta.keys();
		String obj = (String) iter.next();
		if (obj.equals("error")) {
			// setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			return new JsonRepresentation(respuesta.toString());
		} else {
			  setStatus(Status.SUCCESS_OK);
              return null;  
		}
		
	}

	// System.out.println("");

}