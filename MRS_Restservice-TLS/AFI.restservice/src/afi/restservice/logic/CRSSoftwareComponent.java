package afi.restservice.logic;

import java.io.FileNotFoundException;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import afi.restservice.RestActivator;


public class CRSSoftwareComponent extends ServerResource {

	@Get("json")
	public JsonRepresentation getSoftwareComponent() {
		
		JSONObject respuesta = new JSONObject();
		String comp_id = getAttribute("component_id");
		String version = getAttribute("version");
		
		try {
			respuesta = (JSONObject) RestActivator.getSoftwareComponent(comp_id, version);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Iterator<String> iter = respuesta.keys();
		String obj = (String) iter.next();
		if (obj.equals("error")) {
			// setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			return new JsonRepresentation(respuesta.toString());
		} else {
			return new JsonRepresentation(respuesta.toString());

		}
	}

	@Put("json")
	public void saveSoftwareComponent(JsonRepresentation jsonrepresentation) throws JSONException, FileNotFoundException {

		JSONObject respuesta = new JSONObject();
		JSONArray requestJSONArray = jsonrepresentation.getJsonArray();
		JSONObject requestJSON = new JSONObject();

		for (int i = 0; i < requestJSONArray.length(); i++) {

			requestJSON = requestJSONArray.getJSONObject(i);

			String comp_id = requestJSON.getString("Comp_id");
			String version = requestJSON.getString("Version");
			String componente = requestJSON.getString("SWC");
			String extension = requestJSON.getString("Extension");

			respuesta = (JSONObject) RestActivator.saveSoftwareComponent(comp_id, version, componente, extension);

			Iterator<String> iter = respuesta.keys();
			String obj = (String) iter.next();
			if (obj.equals("code")) {
				setStatus(Status.SUCCESS_CREATED);
			}
		}
	}

	@Delete("json")
	public JsonRepresentation deleteSoftwareComponent() {

		JSONObject respuesta = new JSONObject();

		String comp_id = getAttribute("component_id");
		String version = getAttribute("version");

		respuesta = (JSONObject) RestActivator.deleteSoftwareComponent(comp_id, version);

		Iterator<String> iter = respuesta.keys();
		String obj = (String) iter.next();
		if (obj.equals("error")) {
			// setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
			return new JsonRepresentation(respuesta.toString());
		} else {
			setStatus(Status.SUCCESS_OK);
			return null;
		}
	}
}
//System.out.println("");