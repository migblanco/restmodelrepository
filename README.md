In this work, two prototypes of services has been developed (a repository of component models and a 
repository of software components) that will act as warehouses and resource managers. These
resources will be used as part of a tool to support the design and development of self-adaptive
systems. Given the nature of this tool, these repositories should be able to be used to store artifacts
used by the self-adaptive system itself, both in design time and in execution time.
This implementation has been done through a RESTful service where the application will be able to
send the component models and their software components to the repository, which will store them
under a defined directory structure. If in a certain moment the application needs to make use of the
saved files, the repository must supply them. The application can update any previously sent file in
the repository, as well as delete it.
To implement the repositories, a RESTful service has been created, which are web services that conform 
to the REST architectural style and that provide interoperability between computer systems on the Internet.
The repository, the REST web service and the client application have been created as plugins using Java OSGi. 
To create the functionality of the RESTful service we will use Restlet which is a specific Java framework for REST services.