package afi.restlet.client;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;

public class RestletClient {
	private Client client;
	private ClientResource service;
	private static final String keystorePath = "/AFI.Automovil/truststore/clientTrust.jks";
	private String WORKSPACE = "";
	private int port = 0;

	public RestletClient() {

		this.WORKSPACE = System.getProperty("dir.workspace");
		this.port = new Integer(System.getProperty("connect.port")); // port 8183

		this.client = new Client(new Context(), Protocol.HTTPS);

		Series<Parameter> parameters = client.getContext().getParameters();
		parameters.add("truststorePath", WORKSPACE + keystorePath);
		parameters.add("truststorePassword", "mrspass");
		parameters.add("truststoreType", "JKS");

		this.service = new ClientResource("https://127.0.0.1:" + port);
		service.setNext(client);

	}

	public Object sendGetOrDel(String comp_id, String version, String metodo)
			throws Exception, JSONException, ResourceException, IOException {

		ClientResource resource;
		String respuesta = "";
		
			switch (metodo) {
			case "GET":
				resource = service.getChild("/component/" + comp_id + "/" + version);
				respuesta = resource.get().getText();
				break;
			case "DELETE":
				resource = service.getChild("/component/" + comp_id + "/" + version);
				resource.delete();
				respuesta = resource.getStatus().toString();
				break;
			}
			
		

		return respuesta;
}

	public String sendToServer(JSONArray mensaje,String comp_id, String version, String metodo)
			throws IOException {

		ClientResource resource;

	
			switch (metodo) {
			case "PUT":
				resource = service.getChild("/component/" + comp_id + "/" + version);
				resource.put(resource.toRepresentation(mensaje.toString(), MediaType.APPLICATION_JSON));
				return resource.getStatus().toString();
			}
		
		return null;
	}
}
//// ClientResource resourceClient= new ClientResource("http://localhost:8111/component/");






