package afi.automovil;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

public class ManagementFiles {

	public static Object convertFile(String name) throws IOException {

		File file = new File(System.getProperty("java.io.tmpdir") + "/theModel.dat");
		File origin = new File(name);
		byte[] buffer = null;

		try (InputStream inputStream = new FileInputStream(origin);
				OutputStream outputStream = new FileOutputStream(file);) {

			int buffersize = (int) origin.length();
			buffer = new byte[buffersize];

			while (inputStream.read(buffer) != -1) {
				outputStream.write(buffer);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return encodeFile(buffer);
	}

	public static Object encodeFile(byte[] bytes) throws IOException {

		Base64.Encoder mimeEncoder = Base64.getMimeEncoder();
		String encode = mimeEncoder.encodeToString(bytes);
		// System.out.println(encode);
		return encode;
	}
	public static void saveAsBinaryFile(String destPath,Object theModel) throws IOException {
		
		
		Base64.Decoder decoder = Base64.getMimeDecoder();
		byte[] decodedByteArray = decoder.decode(theModel.toString());
		
		
          try {
               FileOutputStream binaryObject = new FileOutputStream(destPath);
               

               binaryObject.write(decodedByteArray);
               binaryObject.close();
         }
        catch(IOException ex)   {
			ex.printStackTrace();
              
        }
      
          System.out.println("Guardado como " + destPath);
     }
}