package afi.automovil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public class ManagementFiles {


	public static Object convertFile(String name) throws IOException {

		File file = new File(System.getProperty("java.io.tmpdir") + "/theModel.dat");
		File origin = new File(name);
		byte[] buffer = null;

		try (InputStream inputStream = new FileInputStream(origin);
				OutputStream outputStream = new FileOutputStream(file);) {

			int buffersize = (int) origin.length();
			buffer = new byte[buffersize];

			while (inputStream.read(buffer) != -1) {
				outputStream.write(buffer);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return encodeFile(buffer);
	}

	public static Object encodeFile(byte[] bytes) throws IOException {

		Base64.Encoder mimeEncoder = Base64.getMimeEncoder();
		String encode = mimeEncoder.encodeToString(bytes);
		// System.out.println(encode);
		return encode;
	}
}

	
	public static String readFile(File f) {
		String fileContent = "";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			String inputLine;
			while((inputLine = br.readLine())!=null){
				fileContent+=inputLine;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileContent;
	}

	
}

