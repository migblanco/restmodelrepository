package afi.automovil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import afi.restlet.client.RestletClient;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator implements BundleActivator {

	private static final String APPNAME = "Smart_Car_17";
	private static final String DIRLOADCM = "/AFI.Automovil/models/CM/";
	private static final String DIRLOADSCM = "/AFI.Automovil/models/SCM/";
	private String WORKSPACE = "";
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	public Activator() {
		this.WORKSPACE = System.getProperty("dir.workspace");
	}

	@SuppressWarnings("unchecked")
	public void start(BundleContext context) throws Exception {

		Activator.context = context;
		RestletClient client = new RestletClient();
		JSONArray objectTosend = new JSONArray();

		objectTosend = sendComponent("1.0.0");
		System.out.println(client.sendToServer(objectTosend, APPNAME, null, "1.0.0", "CM", "PUT"));

		objectTosend = sendComponent("1.0.1");
		System.out.println(client.sendToServer(objectTosend, APPNAME, null, "1.0.1", "CM", "PUT"));
		Thread.sleep(1000);

		objectTosend = sendComponent("1.0.2");
		System.out.println(client.sendToServer(objectTosend, APPNAME, null, "1.0.2", "CM", "PUT"));

		objectTosend = sendComponent("1.2.0");
		System.out.println(client.sendToServer(objectTosend, APPNAME, null, "1.2.0", "CM", "PUT"));

		System.out.println(client.sendGetOrDel("CM", null, "1.2.0", "GET"));
		System.out.println(client.sendGetOrDel("CM", null, "1.8.0", "GET"));

		objectTosend = sendSystemConfigurationModel("SC0_InitialSystemConfiguration", "1.0.1", "1.0.0");
		System.out.println(
				client.sendToServer(objectTosend, APPNAME, "SC0_InitialSystemConfiguration", "1.0.1", "SCM", "PUT"));

		objectTosend = sendSystemConfigurationModel("SC1_OnStdContextSystemConfiguration", "1.0.1", "1.0.0");
		Thread.sleep(1000);
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC1_OnStdContextSystemConfiguration", "1.0.1",
				"SCM", "PUT"));

		objectTosend = sendSystemConfigurationModel("SC3_On3GDetectedSystemConfiguration", "1.0.1", "1.0.0");
		Thread.sleep(1000);
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC3_On3GDetectedSystemConfiguration", "1.0.1",
				"SCM", "PUT"));
		objectTosend = sendSystemConfigurationModel("SC5_OnInClassroomEnvironmentSystemConfiguration", "1.0.1",
				"1.0.0");
		Thread.sleep(1000);
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC5_OnInClassroomEnvironmentSystemConfiguration",
				"1.0.1", "SCM", "PUT"));

		objectTosend = sendSystemConfigurationModel("SC1_OnStdContextSystemConfiguration", "1.0.2", "1.0.0");
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC1_OnStdContextSystemConfiguration", "1.0.2",
				"SCM", "PUT"));
		System.out.println(client.sendGetOrDel("SCM", "SC3_On3GDetectedSystemConfiguration", "1.0.0", "GET"));
		System.out
				.println(client.sendGetOrDel("SCM", "SC5_OnInClassroomEnvironmentSystemConfiguration", "1.0.1", "GET"));

		objectTosend = updateSystemConfigurationModel("SC1_OnStdContextSystemConfiguration", "1.0.1");
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC1_OnStdContextSystemConfiguration", "1.0.1",
				"SCM", "PUT"));

		objectTosend = updateSystemConfigurationModel("SC5_OnInClassroomEnvironmentSystemConfiguration", "1.0.1");
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC5_OnInClassroomEnvironmentSystemConfiguration",
				"1.0.1", "SCM", "PUT"));

		objectTosend = updateConfigurationModel(APPNAME, "SC5_OnInClassroomEnvironmentSystemConfiguration", "1.0.1",
				"1.0.1");
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC5_OnInClassroomEnvironmentSystemConfiguration",
				"1.0.1", "SCM", "PUT"));

		objectTosend = updateConfigurationModel(APPNAME, "SC5_OnInClassroomEnvironmentSystemConfiguration", "1.0.1",
				"1.2.0");
		System.out.println(client.sendToServer(objectTosend, APPNAME, "SC5_OnInClassroomEnvironmentSystemConfiguration",
				"1.0.1", "SCM", "PUT"));
		objectTosend = sendComponent("1.2.0");
		System.out.println(client.sendToServer(objectTosend, APPNAME, null, "1.2.0", "CM", "PUT"));
		Thread.sleep(1000);

		System.out.println(client.sendGetOrDel("CM", null, "1.2.0", "DELETE"));
		System.out.println(client.sendGetOrDel("SCM", "SC1_OnStdContextSystemConfiguration", "1.0.2", "DELETE"));
		System.out.println(client.sendGetOrDel("CM", null, "1.2.0", "GET"));

	}

	@Override
	public void stop(BundleContext context) throws Exception {

		System.out.println("Goodbye World!!");
		Activator.context = null;
		/* - - - - - - - - - */

	}

	public JSONArray sendComponent(String version) throws FileNotFoundException, IOException {

		File root = new File(WORKSPACE + DIRLOADCM);
		File auxBinary = new File(WORKSPACE + DIRLOADCM + "/binary/");
		Object obj = new Object();
		Object binaryobj = new Object();
		Object auxiliar = new Object();

		JSONArray arrayToSend = new JSONArray();
		JSONObject objectToSend = new JSONObject();
		JSONObject binaryToSend = new JSONObject();
		// JSONObject binaryToSend2 = new JSONObject();
		File[] arrayFile = root.listFiles();
		auxiliar = arrayFile[1];

		obj = ManagementFiles.convertFile(auxiliar.toString());

		objectToSend.put("App", APPNAME);
		objectToSend.put("Version", version);
		objectToSend.put("CM", obj);
		objectToSend.put("Extension", ".cm");
		arrayToSend.put(objectToSend);

		File[] binaryDirectory = auxBinary.listFiles();
		for (int i = 0; i < binaryDirectory.length; i++) {

			int pos = binaryDirectory[i].getName().lastIndexOf(".");
			String resp = binaryDirectory[i].getName().substring(pos);
			if (resp.equals(".jar")) {
				auxiliar = binaryDirectory[i];
				binaryobj = ManagementFiles.convertFile(auxiliar.toString());
				binaryToSend.put("App", APPNAME);
				binaryToSend.put("Version", version);
				binaryToSend.put("CM", binaryobj);
				binaryToSend.put("Extension", resp);
				arrayToSend.put(binaryToSend);
			}

		}

		return arrayToSend;
	}

	public JSONArray sendSystemConfigurationModel(String name, String version, String cm_version) throws IOException {

		JSONArray arrayTosend = new JSONArray();
		Object auxiliar = new Object();
		Object theModel = new Object();
		JSONObject objectToSend = new JSONObject();
		File root = new File(WORKSPACE + DIRLOADSCM);

		File[] arrayFile = root.listFiles();

		for (int i = 0; i < arrayFile.length; i++) {

			String fileName = arrayFile[i].getName();

			if ((name + ".systemconfigurationmodel").equals(fileName)) {

				auxiliar = arrayFile[i];

			}

		}

		theModel = ManagementFiles.convertFile(auxiliar.toString());

		objectToSend.put("App", APPNAME);
		objectToSend.put("SCM_name", name);
		objectToSend.put("SCM_version", version);
		objectToSend.put("CM_version", cm_version);
		objectToSend.put("SCM", theModel);
		arrayTosend.put(objectToSend);
		return arrayTosend;

	}

	public JSONArray updateSystemConfigurationModel(String name, String version) throws IOException {
		JSONArray arrayTosend = new JSONArray();
		Object theModel = new Object();
		Object auxiliar = new Object();
		JSONObject objectToSend = new JSONObject();
		File root = new File(WORKSPACE + DIRLOADSCM + "/new-input");

		File[] arrayFile = root.listFiles();

		for (int i = 0; i < arrayFile.length; i++) {

			String fileName = arrayFile[i].getName();

			if ((name + ".systemconfigurationmodel").equals(fileName)) {

				auxiliar = arrayFile[i];

			}

		}
		theModel = ManagementFiles.convertFile(auxiliar.toString());
		objectToSend.put("App", APPNAME);
		objectToSend.put("SCM_name", name);
		objectToSend.put("SCM_version", version);
		objectToSend.put("CM_version", "");
		objectToSend.put("SCM", theModel);

		arrayTosend.put(objectToSend);
		return arrayTosend;
	}

	public JSONArray updateConfigurationModel(String app, String name, String version, String cm_version) {
		JSONArray arrayTosend = new JSONArray();
		JSONObject objectToSend = new JSONObject();

		objectToSend.put("App", app);
		objectToSend.put("SCM_name", name);
		objectToSend.put("SCM_version", version);
		objectToSend.put("CM_version", cm_version);
		objectToSend.put("SCM", "");

		arrayTosend.put(objectToSend);
		return arrayTosend;

	}

	

}
